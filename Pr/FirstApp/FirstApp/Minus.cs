﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Minus
    {
        public void MinusNum(double val1, double val2)
        {
            int nSteps;
            if (val1.ToString().Length > val2.ToString().Length)
            {
                nSteps = val1.ToString().Length - val2.ToString().Length;
                Console.WriteLine($" {val1}");
                Console.WriteLine($"-"); 
                Console.WriteLine($" {"".PadLeft(nSteps, ' ')}{val2}");
                Console.WriteLine("-".PadLeft(val1.ToString().Length + 1, '-'));
                Console.WriteLine($" {val1 - val2}");
            }
            else if (val1.ToString().Length < val2.ToString().Length)
            {
                nSteps = val2.ToString().Length - val1.ToString().Length;
                Console.WriteLine($" {"".PadLeft(nSteps, ' ')}{val1}");
                Console.WriteLine($"-");
                Console.WriteLine($" {val2}");
                Console.WriteLine("-".PadLeft(val2.ToString().Length + 1, '-'));
                Console.WriteLine($" {val1 - val2}");
            }
            else
            {
                Console.WriteLine($" {val1}");
                Console.WriteLine($"-");
                Console.WriteLine($" {val2}");
                Console.WriteLine($"---------");
                Console.WriteLine($" {val1 - val2}");
            }
        }
    }
}
