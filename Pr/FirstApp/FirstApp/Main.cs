﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Main
    {
        public void Calculation()
        {
            char action = ' ';
            string str = Console.ReadLine();
            char[] charArray;
            string[] numbers;
            double n1;
            if (str != "")
            {
                charArray = str.ToCharArray();
                numbers = str.Split('+', '-', '/', '*');
                bool isNumber1 = false;
                bool isNumber2 = false;
                if (numbers.Length > 1)
                {
                    isNumber1 = double.TryParse(numbers[0], out n1);
                    isNumber2 = double.TryParse(numbers[1], out n1);
                }
                if (isNumber1 && isNumber2)
                {
                    double num1 = Convert.ToDouble(numbers[0]);
                    double num2 = Convert.ToDouble(numbers[1]);
                    for (int i = 0; i < str.Length; i++)
                    {
                        if (charArray[i] == '+' || charArray[i] == '-' || charArray[i] == '/' || charArray[i] == '*')
                        {
                            action = charArray[i];
                        }
                    }

                    switch (action)
                    {
                        case '+':
                            Plus plusObject = new Plus();
                            plusObject.PlusNum(num1, num2);
                            break;
                        case '-':
                            Minus minusObject = new Minus();
                            minusObject.MinusNum(num1, num2);
                            break;
                        case '/':
                            Division divObject = new Division();
                            divObject.Divide(num1, num2);
                            break;
                        case '*':
                            Multiple mObject = new Multiple();
                            mObject.Multiplication(num1, num2);
                            break;
                        default:
                            Console.WriteLine("Неизвестное действие!");
                            break;
                    }
                } else
                {
                    Console.WriteLine("Неизвестный ввод");
                }
            } else
            {
                Console.WriteLine("Введите выражение");
            }
                
            
        }
    }
}
