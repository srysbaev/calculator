﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Multiple
    {
        public void Multiplication(double v1, double v2)
        {
            double mResult = v1 * v2;
            string val1 = v1.ToString().Replace(",", "");
            string val2 = v2.ToString().Replace(",", "");

            Console.WriteLine($"{"".PadLeft(v2.ToString().Length + 1, ' ')} {v1}");
            Console.WriteLine("*");
            Console.WriteLine($"{"".PadLeft(v2.ToString().Length + 1, ' ')} {v2}");
            Console.WriteLine($"{"".PadLeft(v2.ToString().Length * 2, '-')}");

            for (int i = val2.Length - 1; i >= 0; i--)
            {
                int number = Convert.ToInt32(val2[i]) - 48;
                Console.WriteLine($"{"".PadRight(i + 1, ' ')}{Convert.ToDouble(val1) * number}");
            }

            Console.WriteLine($"{"".PadLeft(val2.ToString().Length * 2, '-')}");
            Console.WriteLine($" {mResult}");
        }
    }
}
