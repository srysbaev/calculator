﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Division
    {
        public void Divide(double v1, double v2)
        {
            double dResult1 = v1 / v2;
            string dResult = (v1 / v2).ToString();
            string val1 = v1.ToString().Replace(",", "");

            double divider = v2;

            Console.WriteLine($"{v1}|{v2}");
            Console.WriteLine("".PadLeft(val1.Length * 2, '-'));
            if (dResult.Length == 1 || dResult.Replace("0", string.Empty).Length == 1)
            {
                Console.WriteLine($"{dResult1 * divider}|{dResult1}");
                return;
            }
            Console.WriteLine(dResult1);
        }
    }
}
